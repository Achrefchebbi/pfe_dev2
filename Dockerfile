# Utilisez une image de base Odoo
FROM odoo:latest

# Copiez le fichier de configuration Odoo
COPY odoo.conf /etc/odoo/odoo.conf

# Copiez le module bi_hr_attendance_leave_report dans le dossier /mnt/extra-addons de l'image Odoo
COPY . /mnt/extra-addons

